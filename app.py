from flask import Flask, jsonify
from flask_restx import Api, Resource, fields
from flask_jwt_extended import JWTManager
from blacklist import BLACKLIST
from extensions import mongo
# Lauches App and Api
from settings import *
# Resources
from resource.hotel import Hotels, Hotel
from resource.user import Users, User
from resource.auth import Auth

# Config JWT with the app
jwt = JWTManager(app)

# --------------------
# -- Jwt Middleware --
# --------------------
# Check blacklist
@jwt.token_in_blacklist_loader
def check_blacklist(token):
    return token['jti'] in BLACKLIST

# Emit message on Unauthorized token
@jwt.revoked_token_loader
def invalid_token():
    return jsonify({'message': 'You have been logged out.'}), 401 # unauthorized

# -------------------
# ---- Resources ----
# -------------------
api.add_resource(Hotel)
api.add_resource(Hotels)
api.add_resource(User)
api.add_resource(Users)
api.add_resource(Auth)

def create_app():
    return app

# Default Flask's init 
if __name__ == '__main__':
    # Lauches the api with the app
    mongo.init_app(app)
    app.run(debug=True, port=5000)
