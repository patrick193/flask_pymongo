import os
from dotenv import load_dotenv
from flask import Flask, jsonify
from flask_cors import CORS
from flask_restx import Api, Resource, fields

# Load environment variables
load_dotenv()

# ---------------------------
# -- Environment variables --
# ---------------------------
JWT_SECRET_KEY = os.getenv('JWT_SECRET_KEY')
MONGO_URI = os.getenv('MONGO_URI')

# Launches the app
app = Flask(__name__)

# Allow CORS
CORS(app)

# Config app
app.config['MONGO_URI'] = MONGO_URI
app.config['JWT_BLACKLIST_ENABLED'] = True # JWT's list users logged
app.config['JWT_SECRET_KEY'] = JWT_SECRET_KEY

# Config Swagger authorizations
authorizations = {
    'apikey': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'Authorization'
    }
}

# Lauches the api with the app and config Swagger
api = Api(
    app,
    version='1.0',
    title='Flask API',
    description='A simple Flask API with JWT, MonoDB and Swagger',
    authorizations=authorizations,
    security='apikey'
)