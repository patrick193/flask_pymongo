import json
from bson.objectid import ObjectId
from bson.json_util import dumps
from extensions import mongo

class UserDao():
    # Get all users
    def getUsers():
        return json.loads(dumps( mongo.db.user.find() ))

    # Get user by id
    def getUserById(id):
        try:
            return json.loads(dumps( mongo.db.user.find_one({'_id': ObjectId( id )}) ))
        except:
            return None
    
    # Get user by name
    def getUserByName(name):
        return json.loads(dumps( mongo.db.user.find({'login': name}) ))

    # Get user by login
    def getUserByLogin(name):
        return json.loads(dumps( mongo.db.user.find_one({'login': name}) ))

    # Save a new user
    def saveUser(data):
        return mongo.db.user.insert_one(data)

    # Update user
    def updateUser(id, data):
        return mongo.db.user.find_one_and_update({'_id': ObjectId( id )}, {"$set": data})

    # Delete user
    def deleteUser(id):
        return mongo.db.user.find_one_and_delete({'_id': ObjectId( id )})
