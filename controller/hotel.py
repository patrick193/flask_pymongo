from dao.hotel import HotelDao

class HotelController():
    '''
    For route /hotels
    '''
    # Get all hotels
    def getAllHotels():
        # Get all hotels
        hotels = HotelDao.getHotels()
        # Verify if exists users
        if(hotels):
            return hotels, 200
        return { 'messsage': 'Hotel not found' }, 404

    # Create a new hotel
    def postNewHotel(data):
        # Create a new hotel
        HotelDao.saveHotel(data)
        # Parse return from mongo to dict
        data['_id'] = str(data['_id'])
        return { 'message': 'Inserted', 'data': data }, 201

    # Update hotel
    def updateHotel(data):
        # Get hotel id
        hotel_id = data['id']
        # Get hotel data
        data = { 'name': data['name'], 'star': data['star'] }
        # Get hotel by id
        hotel = HotelDao.getHotelById(hotel_id)
        # Verify if exist hotel
        if(hotel):
            # Update hotel
            hotel = HotelDao.updateHotel(hotel_id, data)
            return { 'message': 'Updated', 'id': str(hotel_id) }, 200
        return {'message': 'Hotel not found.'}, 404

    '''
    For route /hotels/<string:hotel_id>
    '''
    # Get hotel by id
    def getHotelById(hotel_id):
        # Get hotel by id
        hotel = HotelDao.getHotelById(hotel_id)
        # Verify if exist hotel
        if(hotel):
            return hotel
        return {'message': 'Hotel not found.'}, 404

    # Delete hotel by id
    def deleteHotel(hotel_id):
        # Get hotel by id
        hotel = HotelDao.getHotelById(hotel_id)
        # Verify if exist hotel
        if(hotel):
            # Delete hotel by id
            hotel = HotelDao.deleteHotel(hotel_id)
            return { 'message': 'Deleted'}, 200
        return {'message': 'Hotel not found.'}, 404

    