from flask_jwt_extended import jwt_required
from flask_restx import Resource, reqparse
from controller.auth import AuthController
from settings import api

# Swagger namespace
login_namespace     = api.namespace('login',    description='Login operations')
logout_namespace    = api.namespace('logout',   description='Logout operations')
register_namespace  = api.namespace('register', description='Register operations')

# Request body
body_attributes = reqparse.RequestParser()
body_attributes.add_argument('login', type=str)
body_attributes.add_argument('pwd',   type=str)

'''
Route for /login
'''
@login_namespace.route('') # Swagger namespace
class Auth(Resource):
    # Log in user
    @classmethod # The @Classmethod is called because won't need self data
    def post(cls):
        # Parse body data
        data = body_attributes.parse_args()
        # Call controller
        return AuthController.login(data)

'''
Route for /logout
'''
@logout_namespace.route('') # Swagger namespace
class Auth(Resource):
    # Logout user
    @jwt_required # Necessary JWT
    def post(self):
        # Call controller
        return AuthController.logout()

'''
Route for /register
'''
@register_namespace.route('') # Swagger namespace
class Auth(Resource):
    # Register 'create' a new user
    def post(self):
        # Parse body data
        data = body_attributes.parse_args()
        # Call controller
        return AuthController.register(data)