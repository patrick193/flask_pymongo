from flask_restx import Resource, reqparse
from controller.user import UserController
from settings import api

# Swagger namespace
user_namespace          = api.namespace('user',         description='User operations')
users_namespace         = api.namespace('users',        description='Users operations')
user_by_name_namespace  = api.namespace('user/by-name', description='User by name operations')

# Request body
body_attributes = reqparse.RequestParser()
body_attributes.add_argument('id',    type=str)
body_attributes.add_argument('login', type=str)
body_attributes.add_argument('pwd',   type=str)

'''
Route for /users
'''
@users_namespace.route('')
class Users(Resource):
    # Get all users
    def get(self):
        # Call controller
        return UserController.getAllUsers()
    
    # Update user
    def put(self):
        # Parse body data
        data = body_attributes.parse_args()
        # Call controller
        return UserController.updateUser(data)

'''
Route for /user/<string:user_id>
'''
@api.doc(params={'user_id': 'User id'}) # Params in swagger
@user_namespace.route('/<string:user_id>') # Swagger namespace
class User(Resource):
    # Get User by id
    def get(self, user_id):
        # Call controller
        return UserController.getUserById(user_id)

    # Delete User by id
    def delete(self, user_id):
        # Call controller
        return UserController.deleteUser(user_id)

'''
Route for /user/by-name/<string:user_name>
'''
@api.doc(params={'user_name': 'User name'}) # Params in swagger
@user_by_name_namespace.route('/<string:user_name>') # Swagger namespace
class User(Resource):
    # Get User by name
    def get(self, user_name):
        # Call controller
        return UserController.getUserByName(user_name)
